#ifndef TRACE_QUEUE_MSG_H
#define TRACE_QUEUE_MSG_H

#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/interprocess/sync/interprocess_condition.hpp>
#include <vector>
#include <functional>

struct trace_queue_msg
{
    enum { CStrSize = 100 };

    trace_queue_msg()
        : message_in(false)
    {}

    inline std::string qanswer_name(){
        return std::string(queueName) + "_answer";
    }

    //Mutex to protect access to the queue
    boost::interprocess::interprocess_mutex mutex;

    //Condition to wait when the queue is empty
    boost::interprocess::interprocess_condition cond_wait_msg;

    //Condition to wait when the queue have new message in
    boost::interprocess::interprocess_condition cond_next_msg;

    //Condition to wait when the answer have new message in
    boost::interprocess::interprocess_condition cond_wait_answer;

    char queueName[CStrSize];

    int dataSize;

    //Is there any message
    bool message_in;

    //Is answer require
    bool isAnswerRequire;

    int answerDataSize;

};

typedef std::vector<unsigned char> tMsgBuffer;
typedef std::function<void(const std::string&, const tMsgBuffer&)> tMsgCallback;


#endif // TRACE_QUEUE_MSG_H
