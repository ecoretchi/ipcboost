#include "ipcSender.h"

#include <iostream>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/ipc/message_queue.hpp>


#include "../include/trace_queue_msg.h"

using namespace boost::interprocess;

IpcSender::IpcSender(const std::string& fileName)
    : pipeName("ipc_rcv:" + fileName)
{

}

IpcSender::IpcSender(const std::string &serviceName, tAnswerCallback callback)
    : pipeName("ipc_rcv:" + serviceName)
    , answerCallback(callback)
{

}

IpcSender::~IpcSender()
{

}

bool IpcSender::send(const std::string &msgName, const std::string &buff, bool isAnswerRequire)
{
    void* vbuff = (void*) (buff.data());
    return send(msgName, vbuff, buff.size(), isAnswerRequire);
}

bool IpcSender::send(const std::string& msgName, void *buff, int buffSize, bool isAnswerRequire)
{
    try{
        std::cout << "open shared memory: " << pipeName << std::endl;

        //Create a shared memory object.
        shared_memory_object shm(open_only, pipeName.c_str(), read_write);

        //Map the whole shared memory in this process
        mapped_region region(shm, read_write);

        //Get the address of the mapped region
        void * addr = region.get_address();

        //Obtain a pointer to the shared structure
        trace_queue_msg * data = static_cast<trace_queue_msg*>(addr);

        scoped_lock<interprocess_mutex> lock(data->mutex);
        if(data->message_in)
            data->cond_next_msg.wait(lock);

        data->isAnswerRequire = isAnswerRequire;
        data->dataSize = buffSize;

        std::snprintf(data->queueName, data->CStrSize, "%s", msgName.c_str());

        message_queue::remove(data->queueName);

        //Open a message queue.
        message_queue mq(create_only, data->queueName, 1, buffSize);
        mq.send(buff, buffSize, 0);

        data->message_in = true;
        data->cond_wait_msg.notify_one();

        if(isAnswerRequire){
            data->cond_wait_answer.wait(lock);

            unsigned int priority;
            message_queue::size_type recvd_size;
            int buffSize = data->answerDataSize;

            auto qaName = data->qanswer_name().c_str();
            //Open a message queue.
            message_queue mq(open_only, qaName);

            //Allocate buffer property size
            rcvAnswerBuff.resize(buffSize);

            //Get buffer pointer
            void* vbuff = reinterpret_cast<void*>(rcvAnswerBuff.data());

            //Receive queued message
            mq.receive(vbuff, buffSize, recvd_size, priority);

            message_queue::remove(qaName);
            onAnswer(rcvAnswerBuff);
        }

    } catch(interprocess_exception &ex){
        std::cout << ex.what() << std::endl;
        return false;
    }
    return true;
}

void IpcSender::onAnswer(const tMsgBuffer &buff)
{
    if(answerCallback)
        answerCallback(buff);
}
