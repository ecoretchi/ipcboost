#ifndef IPCSENDER_H
#define IPCSENDER_H

#include <string>
#include "../include/trace_queue_msg.h"

class IpcSender
{
public:
    typedef std::function<void(const tMsgBuffer&)> tAnswerCallback;

public:
    IpcSender(const std::string& pipeName);
    IpcSender(const std::string& pipeName, tAnswerCallback callback);
    virtual ~IpcSender();

public:
    void onAnswer(tAnswerCallback fn){ answerCallback = fn;}
    bool send(const std::string& msgName, const std::string& buff, bool isAnswerRequire = false);
    bool send(const std::string& msgName, void* buff, int buffSize, bool isAnswerRequire = false);

protected:
    virtual void onAnswer(const tMsgBuffer& buff);

private:
    std::string pipeName;
    tMsgBuffer rcvAnswerBuff;
    tAnswerCallback answerCallback;
};

#endif // IPCSENDER_H
