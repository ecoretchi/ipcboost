#include <iostream>
#include <fstream>
#include "ipcSender.h"
using namespace std;
IpcSender ipcs("test");

int main()
{
//    std::ifstream f("test.png", ios_base::in);
//    f.seekg(0, std::ios::end);
//    size_t size = f.tellg();
//    cout << "file size = " << size << endl;
//    std::string buffer(size, ' ');
//    f.seekg(0);
//    f.read(&buffer[0], size);

    std::ifstream f("test.png");
    std::stringstream buffer;
    buffer << f.rdbuf();

    bool res = ipcs.send("detect", buffer.str());

    f.close();

    cout << "send result = " << boolalpha << res << endl;
    return 0;
}
