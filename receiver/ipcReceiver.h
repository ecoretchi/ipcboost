#ifndef IPCRECEIVER_H
#define IPCRECEIVER_H

#include <atomic>
#include <string>
#include <thread>
#include <mutex>
#include <memory>
#include <vector>
#include <string>
#include <functional>
#include "../include/trace_queue_msg.h"

class IpcReceiver
{
public:
    IpcReceiver(const std::string& pipeName);
    virtual ~IpcReceiver();

public:
    bool receive(const tMsgCallback& callback); // blocked method
    bool stop(); // stop receive, could use if threaded
    bool isRun() const; // check if waiting or receiving
    const std::string& lastError(){ return lastErrText; }
    bool reply(const std::string& answerData); // send answer if necessary

private:
    std::string pipeName;
    trace_queue_msg* data;
    tMsgBuffer buffer;
    std::string lastErrText;
    bool stopSignal;

};

#endif // IPCRECEIVER_H
