#include "ipcReceiver.h"
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/interprocess/sync/interprocess_condition.hpp>
#include <boost/interprocess/ipc/message_queue.hpp>
#include <exception>
#include <iostream>

#include "../include/trace_queue_msg.h"


using namespace boost::interprocess;

namespace {

    struct {
        std::ostream& i = std::cout;
        std::ostream& d = std::cout;
        std::ostream& w = std::cout;
        std::ostream& e = std::cout;
    } logTrace;

}

IpcReceiver::IpcReceiver(const std::string& fileName)
    : pipeName("ipc_rcv:" + fileName)
    , data(nullptr)
    , stopSignal(false)
{
    shared_memory_object::remove(fileName.c_str());
}

IpcReceiver::~IpcReceiver()
{
    stop();
    shared_memory_object::remove(pipeName.c_str());
}

bool IpcReceiver::isRun() const
{
    if(data == nullptr){
        try{
            shared_memory_object shm(open_only, pipeName.c_str(), read_only);
            std::cout << "open!!" << std::endl;
            return true;
        }
        catch(interprocess_exception &ex){
            return false;
        }
    }
    return true;
}

bool IpcReceiver::reply(const std::string &answerData)
{
    lastErrText.clear();

    if(data == nullptr){
        throw std::logic_error("invalid data ptr");
    }

    if(data->isAnswerRequire){
        data->answerDataSize = answerData.size();

        auto qname = data->qanswer_name().c_str();

        try{
            //Open a message queue.
            message_queue mq(create_only, qname, 1, answerData.size());

            mq.send(answerData.data(), answerData.size(), 0);
        }catch(interprocess_exception &ex){
            lastErrText = ex.what();
            data->cond_wait_answer.notify_one();
            return false;
        }

        data->cond_wait_answer.notify_one();
        return true;
    }

    lastErrText = "answer not necessary";
    return false;
}


bool IpcReceiver::receive(const tMsgCallback& callback)
{
    logTrace.i << "IpcReceiver::receive: " << pipeName << std::endl;

    if(isRun()){
        lastErrText = "already run";
        logTrace.e << "err: " << lastErrText << std::endl;
        return false;
    }
    stopSignal = false;
    try{
        logTrace.d << "open shared memory: " << pipeName << std::endl;

        //Create a shared memory object.
        shared_memory_object shm(create_only, pipeName.c_str(), read_write);

        //Set size
        shm.truncate(sizeof(trace_queue_msg));

        //Map the whole shared memory in this process
        mapped_region region(shm ,read_write);

        //Get the address of the mapped region
        void * addr = region.get_address();

        //Construct the shared structure in memory
        data = new (addr) trace_queue_msg;

        do {
            logTrace.d << "wait...\n";

            scoped_lock<interprocess_mutex> lock(data->mutex);
            if(data->message_in == false) // waiting for message income
                data->cond_wait_msg.wait(lock);

            logTrace.d << "process\n";

            if(stopSignal)
                break; // exit point

            unsigned int priority;
            message_queue::size_type recvd_size;
            int buffSize = data->dataSize;

            logTrace.d << "open mq: " << data->queueName << std::endl;

            //Open a message queue.
            message_queue mq(open_only, data->queueName);

            //Allocate buffer property size
            buffer.resize(buffSize);

            logTrace.d << "buffer size: " << buffSize << std::endl;

            //Get buffer pointer
            void* vbuff = reinterpret_cast<void*>(buffer.data());

            //Receive queued message
            mq.receive(vbuff, buffSize, recvd_size, priority);

            logTrace.d << "received buff, size: " << recvd_size << std::endl;

            //Callback received message data
            callback(data->queueName, buffer);

            //Finilize message read
            data->message_in = false;

            //Notify ready to get the next message
            data->cond_next_msg.notify_one();

            //Remove queue message
            message_queue::remove(data->queueName);

        } while(!stopSignal);

    } catch(interprocess_exception &ex){
        lastErrText = ex.what();
        return false;
    }
    boost::interprocess::shared_memory_object::remove(pipeName.c_str());
    data = nullptr;
    return true;
}

bool IpcReceiver::stop()
{
    logTrace.i << "IpcReceiver[" << pipeName << "]::stop()" << std::endl;

    if(isRun()){
        stopSignal = true;
        if(data)
            data->cond_wait_msg.notify_one();
        return true;
    }
    return false;
}

