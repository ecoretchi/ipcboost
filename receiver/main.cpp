#include <iostream>
#include <cstdio>
#include <csignal>

#include "trace_queue.h"
#include "ipcReceiver.h"


IpcReceiver ipcr("test");

void signalHandler( int signum ) {
   std::cout << "Interrupt signal (" << signum << ") received.\n";
   // cleanup and close up stuff here
   ipcr.stop();
   // terminate program
   exit(signum);
}

int main ()
{
    signal(SIGINT, signalHandler);

    bool ret = ipcr.receive([](const std::string& msgName, const tMsgBuffer& buffer){
       std::cout << msgName << std::endl
                 << "buffer.size()=" << buffer.size() << std::endl;
    });
    if(ret == false)
        std::cout <<"err:" <<  ipcr.lastError() << std::endl;

   return 0;
}
